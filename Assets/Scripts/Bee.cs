﻿using System;
using Assets.Scripts.Events;
using Assets.Scripts.Helpers;
using UnityEngine;

namespace Assets.Scripts
{
    /// <summary>
    /// Скрипт пчелы
    /// </summary>
    public class Bee : MonoBehaviour
    {
        [SerializeField]
        private AudioClip mStingClip;


        private void OnTriggerEnter2D(Collider2D other)
        {
            switch (other.gameObject.tag)
            {
                case Constants.Tags.cDeadZone:
                    EventAggregator.Publish(new DiedEvent<Bee>(this));
                    break;
                case Constants.Tags.cCatcher:
                    EventAggregator.Publish(new CaughtEvent<Bee>(this));
                    break;
            }
        }

        public void PlayStingClip()
        {
            AudioSource.PlayClipAtPoint(mStingClip, transform.position);
        }
    }
}
