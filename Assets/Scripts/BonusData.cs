﻿using System;
using Assets.Scripts.Enums;
using UnityEngine;

namespace Assets.Scripts
{
    [Serializable]
    public class BonusData
    {
        [SerializeField]
        private Sprite mIcon;
        public Sprite Icon
        {
            get { return mIcon; }
            set { mIcon = value; }
        }

        [SerializeField]
        private BonusTypes mBonusType;
        public BonusTypes BonusType
        {
            get { return mBonusType; }
            set { mBonusType = value; }
        }

        [SerializeField]
        private float mChance;
        public float Chance
        {
            get { return mChance; }
            set { mChance = value; }
        }
    }
}
