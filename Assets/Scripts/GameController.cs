﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Enums;
using Assets.Scripts.ModeBehaviours;
using Assets.Scripts.UI;
using UnityEngine;
using SmartLocalization;

namespace Assets.Scripts
{
    /// <summary>
    /// Контроллер игры
    /// </summary>
    public class GameController : MonoBehaviour
    {

        #region Inspector Fields

        [SerializeField]
        private Spawner[] mSpawners = new Spawner[4];

        [SerializeField]
        private InputController m_inputController;

        [SerializeField]
        private AudioClip mGameOverClip;

        [SerializeField]
        private GameObject mStatePanelPrefab;

        [SerializeField]
        private GameObject mResultWindowPrefab;

        [SerializeField]
        private Canvas mMainCanvas;

        [SerializeField]
        private Popup mPopup;

        #endregion


        private int mLives;
        /// <summary>
        /// Жизни
        /// </summary>
        public int Lives
        {
            get { return mLives; }
            set
            {
                mLives = Mathf.Max(0, value);
                StatePanel.Lives = Lives.ToString();
                if (Lives <= 0)
                    StartCoroutine(EndGame());
            }
        }

        private int mScore;
        /// <summary>
        /// Очки
        /// </summary>
        public int Score
        {
            get { return mScore; }
            set
            {
                mScore = Mathf.Max(0, value);
                StatePanel.Score = Score.ToString();
            }
        }

        public StatePanel StatePanel { get; private set; }

        private _ModeBehaviour m_modeBehaviour;


        private void Start()
        {
            //панель состояния игры
            var statePanelObj = Instantiate(mStatePanelPrefab);
            statePanelObj.transform.SetParent(mMainCanvas.transform, false);
            StatePanel = statePanelObj.GetComponent<StatePanel>();


            //инициализация игрового режима
            switch (StartScene.SelectedMode)
            {
                    case GameModes.Simple:
                    m_modeBehaviour = GetComponent<SimpleModeBehaviour>();
                    break;
                    
                    case GameModes.Colored:
                    m_modeBehaviour = GetComponent<ColoredModeBehaviour>();
                    break;

                default:
                    throw new ArgumentOutOfRangeException("mGameState.Mode");
            }
            Score = 0;
            Lives = 3;

            m_inputController.CanInput = true;

            m_modeBehaviour.Spawners = mSpawners;
            m_modeBehaviour.enabled = true;
        }
	
        private void Update() 
        {
            //выход из игры
            if (Input.GetKeyDown(KeyCode.Escape))
                Application.Quit();
        }



        public void TimeDilation()
        {
            StartCoroutine(RunTimeDilation());
        }

        public void ShowPopup(Sprite icon, float time, Vector3 position, Quaternion rotation)
        {
            Popup popup = (Popup)Instantiate(mPopup, position, rotation);
            popup.Icon = icon;
            popup.Time = time;
        }

        private IEnumerator RunTimeDilation()
        {
            //todo проверка, что время уже замедлено, вынести числа в константы
            Time.timeScale = 0.5f;
            yield return new WaitForSeconds(3.0f);
            Time.timeScale = 1f;
        }


        private IEnumerator EndGame()
        {
            //отанавливаем игру...
            m_inputController.CanInput = false;
            m_modeBehaviour.OnEndGame();
            Time.timeScale = 1f;
            yield return new WaitForSeconds(2f);
            //выводим результаты
            AudioSource.PlayClipAtPoint(mGameOverClip, transform.position);

            var resultWindowObj = Instantiate(mResultWindowPrefab);
            resultWindowObj.transform.SetParent(mMainCanvas.transform, false);
            var resultWindow = resultWindowObj.GetComponent<ResultWindow>();
            resultWindow.Score = string.Format("{0}: {1}",
                LanguageManager.Instance.GetTextValue(Constants.LocalizationKeys.cScore), Score);
            resultWindow.HighScore = string.Format("{0}: {1}",
                LanguageManager.Instance.GetTextValue(Constants.LocalizationKeys.cHighScore), m_modeBehaviour.HighScore);
        }
    }
}
