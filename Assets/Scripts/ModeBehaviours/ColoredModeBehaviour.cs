﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Enums;
using Assets.Scripts.Events;
using Assets.Scripts.Helpers;
using Assets.Scripts.ObjectPool.Scripts;
using Assets.Scripts.UI;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.ModeBehaviours
{
    /// <summary>
    /// Скрипт цветного режима
    /// </summary>
    class ColoredModeBehaviour : _ModeBehaviour
    {
        [SerializeField]
        private GameObject mColorSequenceWindowPrefab;

        private ColorSequenceWindow mColorSequenceWindow;

        private readonly List<HoneyColors> mCurrentListColors = new List<HoneyColors>();

        private const int cKeyScore = 20;
        private const int cStartColorsCount = 2;
        private const int cColorsStep = 1;

        private int mCurrentColorIndex;
        private HoneyColors mCurrentColor;

        public override int HighScore
        {
            get { return PlayerPrefs.GetInt(HighScoreKey, 0); }
        }

        protected override string HighScoreKey
        {
            get { return "ColoredModeHighscore"; }
        }

        private void OnEnable()
        {
            EventAggregator.Subscribe<DiedEvent<Honey>>(OnHoneyDied);
            EventAggregator.Subscribe<CaughtEvent<Honey>>(OnHoneyCaught);
            IncreaseColors(cStartColorsCount);
        }

        private void OnDisable()
        {
            EventAggregator.Unsubscribe<DiedEvent<Honey>>(OnHoneyDied);
            EventAggregator.Unsubscribe<CaughtEvent<Honey>>(OnHoneyCaught);
        }



        /// <summary>
        /// Метод увеличивает последовательность цветных бочек для поимки
        /// </summary>
        /// <param name="count"></param>
        private void IncreaseColors(byte count)
        {
            IsSpawning = false;
            mCurrentColorIndex = 0;
            for (int i = 0; i < count; i++)
            {
                mCurrentListColors.Add(mCurrentListColors.Count == 0
                    ? EnumHelper.RandomEnumValue<HoneyColors>()
                    //чтобы предыдущий цвет не повторился, исключаем  его из рандома
                    : EnumHelper.RandomEnumValueExcept(mCurrentListColors[mCurrentListColors.Count - 1]));
            }
            //отображаем окно с инфой
            var colorSequenceWindowObj = Instantiate(mColorSequenceWindowPrefab);
            colorSequenceWindowObj.transform.SetParent(MainCanvas.transform, false);
            mColorSequenceWindow = colorSequenceWindowObj.GetComponent<ColorSequenceWindow>();
            mColorSequenceWindow.OnOkButtonClickAction = () =>
            {
                IsSpawning = true;
            };
            foreach (var currentListColor in mCurrentListColors)
            {
                mColorSequenceWindow.AddHoney(currentListColor);
            }
        }

        protected override IEnumerator Spawning()
        {
            yield return new WaitForSeconds(SpawnInterval);
            while (IsSpawning)
            {
                //при превышении определенного числа очков увеличиваем последовательность цветов,
                //а также увеличиваем скорость и уменьшаем интервал спавна
                int rest = mGameController.Score / cKeyScore;
                if (rest > mCurrentListColors.Count - cStartColorsCount)
                {
                    IncreaseColors(cColorsStep);
                    var newEnemySpeed = cBaseObjectSpeed * (1f + 0.2f * rest);
                    if (newEnemySpeed > ObjectSpeed)
                        ObjectSpeed = newEnemySpeed;

                    var newSpawnInterval = cBaseSpawnInterval / (1f + 0.2f * rest);
                    if (newSpawnInterval < SpawnInterval)
                        SpawnInterval = newSpawnInterval;
                }

                //спавн
                int currentSpawnerIndex = Random.Range(0, Spawners.Length);
                mCurrentColor = mCurrentListColors[mCurrentColorIndex];
                for (int i = 0; i < Spawners.Length; i++)
                {
                    Spawner spawner = Spawners[i];
                    Honey honey = spawner.SpawnHoney(ObjectSpeed);
                    if (i == currentSpawnerIndex)
                    {
                        honey.Color = mCurrentColor;
                        if(IsHoneyHaveBonus())
                            honey.GenerateBonus();
                    }
                    else
                    {
                        honey.Color = EnumHelper.RandomEnumValueExcept(mCurrentColor);
                    }
                }
                yield return new WaitForSeconds(SpawnInterval);
            }
        }

        public override void OnHoneyDied(DiedEvent<Honey> diedEvent)
        {
            var honey = diedEvent.Object;
            //отнимаем жизнь, если уронили нужную бочку
            if (honey.Color == mCurrentColor)
            {
                mGameController.Lives--;
                honey.PlayFailedClip();
            }
            honey.Recycle();
        }

        public override void OnHoneyCaught(CaughtEvent<Honey> caughtEvent)
        {
            var honey = caughtEvent.Object;
            //поймали нужную бочку - даем очки и переходим к ловле следующей
            if (honey.Color == mCurrentColor)
            {
                mGameController.Score++;
                mCurrentColorIndex++;
                if (mCurrentColorIndex >= mCurrentListColors.Count)
                    mCurrentColorIndex = 0;
                honey.PlayCatchedClip();
            }
            honey.Recycle();
        }

        public override void OnBeeCaught(CaughtEvent<Bee> caughtEvent)
        {
            var bee = caughtEvent.Object;
            bee.Recycle();
        }

        public override void OnBeeDied(DiedEvent<Bee> diedEvent)
        {
            var bee = diedEvent.Object;
            bee.Recycle();
        }

    }
}
