﻿using System;
using System.Collections;
using Assets.Scripts.Enums;
using Assets.Scripts.Events;
using Assets.Scripts.Helpers;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.ModeBehaviours
{
    /// <summary>
    /// Базовый класс режима игры
    /// </summary>
    public abstract class _ModeBehaviour : MonoBehaviour
    {
        [SerializeField]
        protected Canvas MainCanvas;
        /// <summary>
        /// Коллекция спавнеров
        /// </summary>
        public Spawner[] Spawners { get; set; }
        /// <summary>
        /// Интервал спавна объектов
        /// </summary>
        public float SpawnInterval { get; protected set; }
        /// <summary>
        /// Скорость объектов
        /// </summary>
        public float ObjectSpeed { get; protected set; }
        /// <summary>
        /// Рекорд режима
        /// </summary>
        public abstract int HighScore { get; }

        protected abstract string HighScoreKey { get; }

        protected GameController mGameController;

        protected const float cBaseSpawnInterval = 2.0f;
        protected const float cBaseObjectSpeed = 1.5f;
        protected const float cHoneyBonusChance = 1f;

        private bool mIsSpawning;
        /// <summary>
        /// Производить ли спавн объектов
        /// </summary>
        public bool IsSpawning
        {
            get { return mIsSpawning; }
            protected set
            {
                mIsSpawning = value;
                if (IsSpawning)
                    StartCoroutine(Spawning());
                else
                    StopCoroutine(Spawning());
            }
        }

        private void Awake()
        {
            mGameController = FindObjectOfType<GameController>();
            SpawnInterval = cBaseSpawnInterval;
            ObjectSpeed = cBaseObjectSpeed;
        }

        protected bool IsHoneyHaveBonus()
        {
            return Random.value <= cHoneyBonusChance;
        }

        /// <summary>
        /// Спавнит объекты
        /// </summary>
        /// <returns></returns>
        protected abstract IEnumerator Spawning();

        /// <summary>
        /// Обрабатывает завершение игры
        /// </summary>
        public void OnEndGame()
        {
            IsSpawning = false;

            if (mGameController.Score > HighScore)
                PlayerPrefs.SetInt(HighScoreKey, mGameController.Score);
        }

        protected virtual void HandleHoneyBonus(BonusData bonus, Vector3 position)
        {
            switch (bonus.BonusType)
            {
                case BonusTypes.ExtraLife:
                    mGameController.Lives++;
                    break;
                case BonusTypes.TimeDilation:
                    mGameController.TimeDilation();
                    break;
                case BonusTypes.DoubleReward:
                    mGameController.Score++;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            mGameController.ShowPopup(bonus.Icon, 1f, position, Quaternion.identity);
        }

        public abstract void OnBeeDied(DiedEvent<Bee> diedEvent);

        public abstract void OnBeeCaught(CaughtEvent<Bee> caughtEvent);

        public abstract void OnHoneyDied(DiedEvent<Honey> diedEvent);

        public abstract void OnHoneyCaught(CaughtEvent<Honey> caughtEvent);


    }
}
