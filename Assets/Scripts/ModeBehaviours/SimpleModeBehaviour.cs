﻿using System;
using System.Collections;
using Assets.Scripts.Events;
using Assets.Scripts.Helpers;
using Assets.Scripts.ObjectPool.Scripts;
using Assets.Scripts.UI;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.ModeBehaviours
{
    class SimpleModeBehaviour : _ModeBehaviour
    {
        [SerializeField]
        private GameObject mSimpleModeWindowPrefab;
        private const int cKeyScore = 5;
        private const float cBeeChance = 0.2f;

        public override int HighScore
        {
            get
            {
                return PlayerPrefs.GetInt(HighScoreKey, 0);
            }
        }

        protected override string HighScoreKey
        {
            get { return "SimpleModeHighscore"; }
        }

        private void OnEnable()
        {
            EventAggregator.Subscribe<DiedEvent<Bee>>(OnBeeDied);
            EventAggregator.Subscribe<CaughtEvent<Bee>>(OnBeeCaught);
            EventAggregator.Subscribe<DiedEvent<Honey>>(OnHoneyDied);
            EventAggregator.Subscribe<CaughtEvent<Honey>>(OnHoneyCaught);
            var simpleModeWindowObj = Instantiate(mSimpleModeWindowPrefab);
            simpleModeWindowObj.transform.SetParent(MainCanvas.transform, false);
            var simpleModeWindow = simpleModeWindowObj.GetComponent<SimpleModeWindow>();
            simpleModeWindow.OnOkButtonClickAction = () =>
            {
                IsSpawning = true; 
            };
        }

        private void OnDisable()
        {
            EventAggregator.Unsubscribe<DiedEvent<Bee>>(OnBeeDied);
            EventAggregator.Unsubscribe<CaughtEvent<Bee>>(OnBeeCaught);
            EventAggregator.Unsubscribe<DiedEvent<Honey>>(OnHoneyDied);
            EventAggregator.Unsubscribe<CaughtEvent<Honey>>(OnHoneyCaught);
        }

        protected override IEnumerator Spawning()
        {
            yield return new WaitForSeconds(SpawnInterval);
            while (IsSpawning)
            {
                //при превышении определенного числа очков увеличиваем скорость и уменьшаем интервал спавна
                int rest = mGameController.Score / cKeyScore;
                if (rest != 0)
                {
                    //todo общий метод изменения скорости и интервала для обоих режимов
                    var newEnemySpeed = cBaseObjectSpeed * (1f + 0.2f * rest);
                    if (newEnemySpeed > ObjectSpeed)
                        ObjectSpeed = newEnemySpeed;

                    var newSpawnInterval = cBaseSpawnInterval / (1f + 0.2f * rest);
                    if (newSpawnInterval < SpawnInterval)
                        SpawnInterval = newSpawnInterval;
                }


                //спавним бочку или пчелу
                var indexOfNextSpawner = Random.Range(0, Spawners.Length);

                if (Random.value <= cBeeChance)
                {
                    Spawners[indexOfNextSpawner].SpawnBee(ObjectSpeed);
                }
                else
                {
                    Honey honey = Spawners[indexOfNextSpawner].SpawnHoney(ObjectSpeed);
                    if(IsHoneyHaveBonus())
                        honey.GenerateBonus();
                }
                yield return new WaitForSeconds(SpawnInterval);
            }
        }

        public override void OnHoneyDied(DiedEvent<Honey> diedEvent)
        {
            var honey = diedEvent.Object;
            //отнимаем жизни при потере бочки
            honey.PlayFailedClip();
            mGameController.Lives--;
            honey.Recycle();
        }

        public override void OnHoneyCaught(CaughtEvent<Honey> caughtEvent)
        {
            var honey = caughtEvent.Object;
            //даем очки при поимке бочки
            honey.PlayCatchedClip();
            mGameController.Score++;
            if(honey.IsHaveBonus)
                HandleHoneyBonus(honey.Bonus, honey.transform.position);
            honey.Recycle();
        }

        public override void OnBeeCaught(CaughtEvent<Bee> caughtEvent)
        {
            var bee = caughtEvent.Object;
            //отнимаем очки при поимке пчелы
            bee.PlayStingClip();
            mGameController.Score--;
            bee.Recycle();
        }

        public override void OnBeeDied(DiedEvent<Bee> diedEvent)
        {
            var bee = diedEvent.Object;
            bee.Recycle();
        }
    }
}
