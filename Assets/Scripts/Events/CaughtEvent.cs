﻿
namespace Assets.Scripts.Events
{
    public class CaughtEvent<T>
    {
        public T Object { get; private set; }

        public CaughtEvent(T obj)
        {
            Object = obj;
        }
    }
}
