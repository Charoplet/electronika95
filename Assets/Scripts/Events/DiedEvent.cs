﻿namespace Assets.Scripts.Events
{
    public class DiedEvent<T>
    {
        public T Object { get; private set; }

        public DiedEvent(T obj)
        {
            Object = obj;
        }
    }
}
