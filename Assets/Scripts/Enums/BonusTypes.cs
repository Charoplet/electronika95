﻿namespace Assets.Scripts.Enums
{
    /// <summary>
    /// Виды бонусов
    /// </summary>
    public enum BonusTypes
    {
        ExtraLife,
        TimeDilation,
        DoubleReward,
    }
}
