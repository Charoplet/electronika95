﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Enums
{
    public enum ScreenPositions
    {
        RightTop,
        LeftTop,
        LeftBottom,
        RightBottom
    }
}
