﻿

namespace Assets.Scripts.Enums
{
    /// <summary>
    /// Игровые режимы
    /// </summary>
    public enum GameModes
    {
        Simple,
        Colored
    }
}
