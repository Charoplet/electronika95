﻿namespace Assets.Scripts.Enums
{
    /// <summary>
    /// НАправления движения объектов
    /// </summary>
    enum Directions
    {
        LeftToRight,
        RightToLeft
    }
}
