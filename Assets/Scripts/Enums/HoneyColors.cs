﻿
namespace Assets.Scripts.Enums
{
    /// <summary>
    /// Цвета бочек с мёдом
    /// </summary>
    public enum HoneyColors
    {
        Brown,
        Yellow,
        Red,
        Green,
        Blue
    }
}
