﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Enums;

public class Catcher : MonoBehaviour
{
    [SerializeField]
    private ScreenPositions m_screenPosition;

    public ScreenPositions ScreenPosition { get { return m_screenPosition; } }
}
