﻿using UnityEngine;

namespace Assets.Scripts.Helpers
{
    /// <summary>
    /// Хелперы для работы со цветом
    /// </summary>
    public static class ColorHelper
    {
        public static Color ColorFromRGBA(byte r, byte g, byte b, byte a = 255)
        {
            return new Color(r / 255f, g / 255f, b / 255f, a / 255f);
        }
    }
}
