﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Assets.Scripts.Helpers
{
    /// <summary>
    /// Хелперы для перечислений
    /// </summary>
    public static class EnumHelper
    {
        private static readonly Random mRandom = new Random();

        public static string DescriptionAttr<T>(this T source)
        {
            FieldInfo fi = source.GetType().GetField(source.ToString());

            var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return attributes.Length > 0 
                ? attributes[0].Description 
                : source.ToString();
        }

        
        public static T RandomEnumValue<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>().OrderBy(x => mRandom.Next()).FirstOrDefault();
        }

        public static T RandomEnumValueExcept<T>(params T[] exceptedMembers)
        {
            return Enum.GetValues(typeof(T)).Cast<T>().Except(exceptedMembers).OrderBy(x => mRandom.Next()).FirstOrDefault();
        }
    }
}
