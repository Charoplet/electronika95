﻿using System.Collections.Generic;

namespace Assets.Scripts.Helpers
{
    /// <summary>
    /// Класс для получения значений в зависимости от вероятности
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ChanceHelper<T> : Dictionary<T, float>
    {
        public T GetValue(float chance)
        {
            T result = default(T);
            float currentValue = 0f;
            foreach (var pair in this)
            {
                currentValue += pair.Value;
                if (chance <= currentValue)
                {
                    result = pair.Key;
                    break;
                }
            }
            return result;
        }
    }
}
