﻿using UnityEngine.UI;

namespace Assets.Scripts.Helpers
{
    /// <summary>
    /// Методы расширения для Button
    /// </summary>
    public static class ButtonExtensions
    {
        public static Text GetTextComponent(this Button button)
        {
            return button.transform.GetComponentInChildren<Text>();
        }
    }
}
