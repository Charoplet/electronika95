﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Helpers
{
    public static class EventAggregator
    {
        private static class EventsHolder<T>
        {
            public static readonly Dictionary<Type, Action<T>> s_events = new Dictionary<Type, Action<T>>();
        }

        public static void Subscribe<T>(Action<T> action)
        {
            var type = typeof(T);
            if (EventsHolder<T>.s_events.ContainsKey(type))
                EventsHolder<T>.s_events[type] += action;
            else
                EventsHolder<T>.s_events.Add(type, action);
        }

        public static void Unsubscribe<T>(Action<T> action)
        {
            var type = typeof(T);
            if (EventsHolder<T>.s_events.ContainsKey(type))
                EventsHolder<T>.s_events[type] -= action;
            else
                EventsHolder<T>.s_events.Remove(type);
        }

        public static void Publish<T>(T eventData)
        {
            var type = typeof(T);
            if (EventsHolder<T>.s_events.ContainsKey(type))
                EventsHolder<T>.s_events[type](eventData);
        }
    }
}
