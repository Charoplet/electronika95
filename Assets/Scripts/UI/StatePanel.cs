﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    /// <summary>
    /// Панель состояния игры
    /// </summary>
    public class StatePanel : MonoBehaviour
    {
        [SerializeField]
        private Text m_LivesCtl;
        [SerializeField]
        private Text m_ScoreCtl;

        public string Lives
        {
            get { return m_LivesCtl.text; }
            set
            {
                m_LivesCtl.text = value;
            }
        }

        public string Score
        {
            get { return m_ScoreCtl.text; }
            set
            {
                m_ScoreCtl.text = value;
            }
        }

        public void OnPauseBtnClick()
        {
            Time.timeScale = Time.timeScale == 0f
                ? 1f
                : 0f;
        }
    }
}
