﻿using System;
using Assets.Scripts.Enums;
using Assets.Scripts.Helpers;
using Assets.Scripts.ObjectPool.Scripts;
using SmartLocalization;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    /// <summary>
    /// Окно с информацией о цветном режиме
    /// </summary>
    public class ColorSequenceWindow : MonoBehaviour
    {
        [SerializeField]
        private RectTransform mSequencePanel;
        [SerializeField]
        private GameObject mHoneyPrefab;
        [SerializeField]
        private Text mSequenceTxt;
        [SerializeField]
        private Button mOkBtn;

        public Action OnOkButtonClickAction { get; set; }

        private void Start()
        {
            mHoneyPrefab.CreatePool();
            mSequenceTxt.text = LanguageManager.Instance.GetTextValue(Constants.LocalizationKeys.cSequenceDescription);
            mOkBtn.GetTextComponent().text = LanguageManager.Instance.GetTextValue(Constants.LocalizationKeys.cOk);
        }

        public void AddHoney(HoneyColors honeyColor)
        {
            var honeyObj = mHoneyPrefab.Spawn();
            honeyObj.transform.SetParent(mSequencePanel.transform);
            var honey = honeyObj.GetComponent<Image>();
            honey.color = Honey.GetUnityColor(honeyColor);
        }

        public void OkButtonClick()
        {
            Destroy(gameObject);
            OnOkButtonClickAction();
        }
    }
}
