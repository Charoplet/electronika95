﻿using SmartLocalization;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Helpers;

namespace Assets.Scripts.UI
{
    /// <summary>
    /// Окно результатов
    /// </summary>
    public class ResultWindow : MonoBehaviour
    {
        [SerializeField]
        private Text mScoreCtl;
        [SerializeField]
        private Text mHighScoreCtl;
        [SerializeField]
        private Text mGameOverTxt;
        [SerializeField]
        private Button mTryAgainBtn;
        [SerializeField]
        private Button mMainMenuBtn;

        public string Score
        {
            get { return mScoreCtl.text; }
            set
            {
                mScoreCtl.text = value;
            }
        }


        public string HighScore
        {
            get { return mHighScoreCtl.text; }
            set
            {
                mHighScoreCtl.text = value;
            }
        }

        private void Start()
        {
            mGameOverTxt.text = LanguageManager.Instance.GetTextValue(Constants.LocalizationKeys.cGameOver);
            mTryAgainBtn.GetTextComponent().text = LanguageManager.Instance.GetTextValue(Constants.LocalizationKeys.cTryAgain);
            mMainMenuBtn.GetTextComponent().text = LanguageManager.Instance.GetTextValue(Constants.LocalizationKeys.cMainMenu);
        }


        public void MainMenuClick()
        {
            Application.LoadLevel(0);
        }

        public void PlayAgainClick()
        {
            Application.LoadLevel(1);
        }
    }
}
