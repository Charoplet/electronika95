﻿using Assets.Scripts.Helpers;
using SmartLocalization;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    /// <summary>
    /// Окно настроек
    /// </summary>
    public class SettingsWindow : MonoBehaviour
    {
        public const string cSoundVolumeKey = "SoundVolume";

        [SerializeField]
        private Slider mSoundVolume;
        [SerializeField]
        private Button mOkBtn;
        [SerializeField]
        private Button mCancelBtn;
        [SerializeField]
        private Text mSoundVolumeTxt;

        private void Start()
        {
            mSoundVolume.value = PlayerPrefs.GetFloat(cSoundVolumeKey, 1f);
            mOkBtn.GetTextComponent().text = LanguageManager.Instance.GetTextValue(Constants.LocalizationKeys.cOk);
            mCancelBtn.GetTextComponent().text = LanguageManager.Instance.GetTextValue(Constants.LocalizationKeys.cCancel);
            mSoundVolumeTxt.text = LanguageManager.Instance.GetTextValue(Constants.LocalizationKeys.cSoundVolume);
        }


        public void OkButtonClick()
        {
            PlayerPrefs.SetFloat(cSoundVolumeKey, mSoundVolume.value);
            AudioListener.volume = mSoundVolume.value;
            Destroy(gameObject);
        }


        public void CancelButtonClick()
        {
            Destroy(gameObject);
        }
    }
}
