﻿using System;
using Assets.Scripts.Helpers;
using SmartLocalization;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    /// <summary>
    /// Окно с информацией о простом режиме
    /// </summary>
    public class SimpleModeWindow : MonoBehaviour 
    {
        [SerializeField]
        private Text mSimpleTitleTxt;
        [SerializeField]
        private Button mOkBtn;

        public Action OnOkButtonClickAction { get; set; }

        public void OkButtonClick()
        {
            Destroy(gameObject);
            OnOkButtonClickAction();
        }

        private void Start()
        {
            mSimpleTitleTxt.text = LanguageManager.Instance.GetTextValue(Constants.LocalizationKeys.cSimpleDescription);
            mOkBtn.GetTextComponent().text = LanguageManager.Instance.GetTextValue(Constants.LocalizationKeys.cOk);
        }
    }
}
