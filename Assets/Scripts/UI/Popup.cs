﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Popup : MonoBehaviour
{
    [SerializeField]
    private Image mImageCtl;

    public Sprite Icon
    {
        get { return mImageCtl.sprite; }
        set
        {
            mImageCtl.sprite = value;
        }
    }

    public float Time { get; set; }

    private void Awake()
    {
        Time = 2f;
    }

    private void Start()
    {
        Destroy(gameObject, Time);
    }
}
