﻿using System.Linq;
using Assets.Scripts.Enums;
using UnityEngine;

namespace Assets.Scripts
{
    public class InputController : MonoBehaviour
    {

        [SerializeField]
        private Catcher[] m_catchers = new Catcher[4];
        [SerializeField]
        private GameObject m_bear;


        private Animator m_bearAnimator;
        private readonly int m_screenHalfWidth = Screen.width / 2;
        private readonly int m_screenHalfHeight = Screen.height / 2;
        private readonly int m_isUpAnimHash = Animator.StringToHash("IsUp");

        public bool CanInput { get; set; }

        private void Start()
        {
            //аниматор медведя
            m_bearAnimator = m_bear.GetComponent<Animator>();
            //инициализация объектов, которые ловят
            for (int i = 1; i < m_catchers.Length; i++)
            {
                m_catchers[i].gameObject.SetActive(false);
            }
        }
	

        private void Update()
        {
            //ничего не делам, если юзер не может управлять
            if (!CanInput || !Input.GetMouseButtonDown(0))
                return;

            //в зависимсоти от точки нажатия активируем кэтчер и др.
            Vector3 mousePosition = Input.mousePosition;
            foreach (var catcher in m_catchers)
            {
                catcher.gameObject.SetActive(false);
            }
            if (mousePosition.x > m_screenHalfWidth && mousePosition.y > m_screenHalfHeight)
                HandleInput(ScreenPositions.RightTop);
            else if (mousePosition.x < m_screenHalfWidth && mousePosition.y > m_screenHalfHeight)
                HandleInput(ScreenPositions.LeftTop);
            else if (mousePosition.x < m_screenHalfWidth && mousePosition.y < m_screenHalfHeight)
                HandleInput(ScreenPositions.LeftBottom);
            else if (mousePosition.x > m_screenHalfWidth && mousePosition.y < m_screenHalfHeight)
                HandleInput(ScreenPositions.RightBottom);

        }

        private void HandleInput(ScreenPositions position)
        {
            m_catchers.First(c => c.ScreenPosition == position).gameObject.SetActive(true);
            bool isUp = position == ScreenPositions.LeftTop || position == ScreenPositions.RightTop;
            m_bearAnimator.SetBool(m_isUpAnimHash, isUp);
            float localScale = position == ScreenPositions.RightBottom || position == ScreenPositions.RightTop
                ? 1f
                : -1f;
            m_bear.transform.localScale = new Vector3(localScale, m_bear.transform.localScale.y);
        }
    }
}
