﻿namespace Assets.Scripts
{
    /// <summary>
    /// Игровые константы
    /// </summary>
    class Constants
    {
        /// <summary>
        /// Ключи для локализации
        /// </summary>
        public static class LocalizationKeys
        {
            public const string cColoredMode = "ColoredMode";
            public const string cExitGame = "ExitGame";
            public const string cSettings = "Settings";
            public const string cSimpleMode = "SimpleMode";

            public const string cOk = "Ok";
            public const string cCancel = "Cancel";

            public const string cSoundVolume = "SoundVolume";

            public const string cGameName = "GameName";

            public const string cSequenceDescription = "SequenceDescription";
            public const string cSimpleDescription = "SimpleDescription";
            public const string cScore = "Score";
            public const string cHighScore = "HighScore";

            public const string cGameOver = "GameOver";
            public const string cTryAgain = "TryAgain";
            public const string cMainMenu = "MainMenu";
        }

        /// <summary>
        /// Игровые теги
        /// </summary>
        public static class Tags
        {
            public const string cDeadZone = "DeadZone";
            public const string cCatcher = "Catcher";
            public const string cGameController = "GameController";
        }
    }
}
