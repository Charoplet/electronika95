﻿using System;
using System.Linq;
using Assets.Scripts.Enums;
using Assets.Scripts.Events;
using Assets.Scripts.Helpers;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    /// <summary>
    /// Скрипт бочки с мёдом
    /// </summary>
    public class Honey : MonoBehaviour
    {
        [SerializeField]
        private AudioClip mCatchedClip;

        [SerializeField]
        private AudioClip mFailedClip;

        [SerializeField]
        private BonusData[] mBonuses = new BonusData[3];

        private BonusData mBonus;
        public BonusData Bonus
        {
            get { return mBonus; }
            private set
            {
                mBonus = value;
                mBonusParticles.enableEmission = Bonus != null;
            }
        }

        private HoneyColors mColor;
        public HoneyColors Color
        {
            get { return mColor; }
            set
            {
                mColor = value;
                mSpriteRenderer.color = GetUnityColor(mColor);
            }
        }

        public bool IsHaveBonus {get { return Bonus != null; } }

        private ChanceHelper<BonusData> mBonusProbabilyties; 
        private SpriteRenderer mSpriteRenderer;
        private ParticleSystem mBonusParticles;

        private void Awake()
        {
            mBonusParticles = GetComponent<ParticleSystem>();
            mSpriteRenderer = GetComponent<SpriteRenderer>();
            mBonusProbabilyties = new ChanceHelper<BonusData>();
            foreach (var bonusData in mBonuses)
            {
                mBonusProbabilyties.Add(bonusData, bonusData.Chance);
            }
            if (Math.Abs(mBonusProbabilyties.Values.Sum() - 1f) > float.Epsilon)
                throw new Exception("Bonus chances sum not equals 1");
            Reset();
        }

        private void OnDisable()
        {
            Reset();
        }

        private void Reset()
        {
            Bonus = null;
            Color = HoneyColors.Brown;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            switch (other.gameObject.tag)
            {
                case Constants.Tags.cDeadZone:
                    EventAggregator.Publish(new DiedEvent<Honey>(this));
                    break;
                case Constants.Tags.cCatcher:
                    EventAggregator.Publish(new CaughtEvent<Honey>(this));
                    break;
            }
        }

        public void PlayCatchedClip()
        {
            AudioSource.PlayClipAtPoint(mCatchedClip, transform.position);
        }

        public void PlayFailedClip()
        {
            AudioSource.PlayClipAtPoint(mFailedClip, transform.position);
        }

        public void GenerateBonus()
        {
            float random = Random.value;
            Bonus = mBonusProbabilyties.GetValue(random);
        }


        public static Color GetUnityColor(HoneyColors honeyColor)
        {
            var uColor = new Color();
            switch (honeyColor)
            {
                case HoneyColors.Blue:
                    uColor = ColorHelper.ColorFromRGBA(16, 132, 246);
                    break;
                case HoneyColors.Brown:
                    uColor = ColorHelper.ColorFromRGBA(229, 152, 37);
                    break;
                case HoneyColors.Green:
                    uColor = ColorHelper.ColorFromRGBA(13, 180, 28);
                    break;
                case HoneyColors.Red:
                    uColor = ColorHelper.ColorFromRGBA(232, 29, 29);
                    break;
                case HoneyColors.Yellow:
                    uColor = ColorHelper.ColorFromRGBA(255, 227, 0);
                    break;
            }
            return uColor;
        }
    }
}
