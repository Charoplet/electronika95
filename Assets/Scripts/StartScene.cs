﻿using Assets.Scripts;
using Assets.Scripts.Enums;
using Assets.Scripts.Helpers;
using Assets.Scripts.UI;
using SmartLocalization;
using UnityEngine;
using UnityEngine.UI;
using UnityEngineInternal;

/// <summary>
/// Скрипт стартовой сцены
/// </summary>
public class StartScene : MonoBehaviour
{
    [SerializeField]
    private Canvas mCanvas;
    [SerializeField]
    private GameObject mSettingsWindowPrefab;
    [SerializeField]
    private Button mSimpleModeBtn;
    [SerializeField]
    private Button mColoredModeBtn;
    [SerializeField]
    private Button mSettingsBtn;
    [SerializeField]
    private Button mExitBtn;
    [SerializeField]
    private Text mGameNameTxt;

    public static GameModes SelectedMode { get; private set; }

    private void Start()
    {
        AudioListener.volume = PlayerPrefs.GetFloat(SettingsWindow.cSoundVolumeKey, 1f);
        mSimpleModeBtn.GetTextComponent().text = LanguageManager.Instance.GetTextValue(Constants.LocalizationKeys.cSimpleMode);
        mColoredModeBtn.GetTextComponent().text = LanguageManager.Instance.GetTextValue(Constants.LocalizationKeys.cColoredMode);
        mSettingsBtn.GetTextComponent().text = LanguageManager.Instance.GetTextValue(Constants.LocalizationKeys.cSettings);
        mExitBtn.GetTextComponent().text = LanguageManager.Instance.GetTextValue(Constants.LocalizationKeys.cExitGame);
        mGameNameTxt.text = LanguageManager.Instance.GetTextValue(Constants.LocalizationKeys.cGameName);
    }

    private void Update()
    {
        //выход из игры
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }

    /// <summary>
    /// Играть в простой режим
    /// </summary>
    public void PlaySimpleButtonClick()
    {
        SelectedMode = GameModes.Simple;
        Application.LoadLevel(1);
    }
    /// <summary>
    /// Играть в цветной режим
    /// </summary>
    public void PlayColoredButtonClick()
    {
        SelectedMode = GameModes.Colored;
        Application.LoadLevel(1);
    }
    /// <summary>
    /// Настройки
    /// </summary>
    public void SettingsButtonClick()
    {
        var settingsWindow = Instantiate(mSettingsWindowPrefab);
        settingsWindow.transform.SetParent(mCanvas.transform, false);
    }
    /// <summary>
    /// Выход
    /// </summary>
    public void ExitGameClick()
    {
        Application.Quit();
    }

}
