﻿using Assets.Scripts.ObjectPool.Scripts;
using System;
using Assets.Scripts.Enums;
using UnityEngine;

namespace Assets.Scripts
{
    /// <summary>
    /// Скрипт спавнера
    /// </summary>
    public class Spawner : MonoBehaviour
    {
        [SerializeField]
        private GameObject mHoneyPrefab;
        [SerializeField]
        private GameObject mBeePrefab;
        [SerializeField]
        private Transform mStartSpawn;
        [SerializeField]
        private Directions mSpawnDirection;

        private void Start()
        {
            mHoneyPrefab.CreatePool();
            mBeePrefab.CreatePool();
        }

        public Honey SpawnHoney(float speed)
        {
            var honey = mHoneyPrefab.Spawn(mStartSpawn.position, Quaternion.identity);
            SetSpeedAndTurn(honey, speed);
            return honey.GetComponent<Honey>();
        }

        public Bee SpawnBee(float speed)
        {
            var bee = mBeePrefab.Spawn(mStartSpawn.position, Quaternion.identity);
            SetSpeedAndTurn(bee, speed);
            return bee.GetComponent<Bee>();
        }

        /// <summary>
        /// Задаем вектор скорости и поворачиваем объект в зависимости от направления движения
        /// </summary>
        /// <param name="gameObj"></param>
        /// <param name="speed"></param>
        private void SetSpeedAndTurn(GameObject gameObj, float speed)
        {
            var speedVector = new Vector2(GetSpeedByDirection(speed, mSpawnDirection), 0);
            gameObj.GetComponent<Rigidbody2D>().velocity = speedVector;
            var localScale = gameObj.transform.localScale;
            int scaleX = speedVector.x > 0 ? 1 : -1;
            gameObj.transform.localScale = new Vector3(Mathf.Abs(localScale.x) * scaleX, localScale.y, localScale.z);
        }

        private float GetSpeedByDirection(float speed, Directions direction)
        {
            switch (direction)
            {
                case Directions.LeftToRight:
                    return speed;
                case Directions.RightToLeft:
                    return -speed;
                default:
                    throw new ArgumentOutOfRangeException("direction");
            }
        }
    }
}
